classdef piece
    %PIECE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        type
        color
        i
        j
    end
    
    methods
        function obj = piece(type,color, i,j)
            %PIECE Construct an instance of this class
            %   Construct a piece alredy detected
            obj.type= type;
            obj.color= color;
            obj.j= j;
            obj.i= i;
        end
        
    end
end

